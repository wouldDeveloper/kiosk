��    #     4	      L      L     M     R     g      �     �     �     �     �     �     �                         +  	   4     >     M     S  
   Z     e     m     u     �     �     �     �     �     �     �     �     �     �     �     �               4     R     f     �     �     �     �     �     �     �     �     �     �  	   �     �     �          
       
   #     .  )   ?  >   i  :   �  @   �  '   $  /   L     |     �     �     �     �     �     �  K   �  G     g   b  .   �     �            	   9     C  
   K     V     \     p     x     �  
   �     �     �     �     �     �     �     �  	   �  
   �               .     3     <     E     I     Y  U   g     �     �  �   �     h     n  W   w  Y   �     )     .  	   4     >     E     Y     ^     c     k          �     �     �     �     �  '   �  $   �          7     K     c     s     �     �     �  	   �     �     �  
   �     �       	     
        *     1     =     F     R     V     [     _     k     s     x  	   �  	   �  	   �     �     �     �  	   �     �               #     >     J  L   [     �     �     �     �     �     �     �       
        '     .     7     ?     L  h   S     �  
   �  	   �     �                 #      1      ?      E      X      k      x      �      �      �      �      �      �      �      �      �   
   !     !     !     !     (!  a   :!     �!     �!     �!     �!     �!     �!     �!     �!     "     "     ."     6"     Q"     d"  
   z"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     #  	   .#     8#     A#     J#     O#     d#  \   x#     �#     �#     �#     �#     �#     �#     $      $     )$  
   C$  7   N$     �$     �$     �$     �$     �$     �$     �$     �$     �$     �$     	%     %     %     4%  -   C%  8   q%  "   �%  
   �%  	   �%     �%  $   �%  
   &     "&      >&     _&     k&     n&     �&     �&     �&     �&     �&     �&  �  �&     �(  D   �(  /   �(  '   ()     P)     h)     ~)  /   �)  )   �)     �)     *     *     &*     <*     R*     a*     h*  
   z*     �*     �*     �*  
   �*     �*     �*     �*     +     )+     8+      A+  !   b+     �+  "   �+     �+     �+     �+     �+  X   ,  3   o,  )   �,  '   �,     �,     �,     -     )-  
   6-     A-     F-  $   \-     �-  	   �-     �-     �-     �-     �-     �-     �-     .     .  L   ..  P   {.  h   �.  _   5/  F   �/  8   �/  E   0     [0     b0     v0  !   �0  ,   �0  "   �0  p   �0  m   h1  �   �1  M   2     �2     �2  D   �2     @3  
   S3     ^3  	   u3     3     �3     �3  #   �3     �3     �3  
   4     4     4     44     A4     N4     n4     �4  '   �4  -   �4     �4     5     /5     N5     b5     r5  g   �5     �5     �5     6  	   7     7  z   7  �   �7     &8     /8     ?8     U8     d8     z8     �8     �8     �8     �8     �8     �8     �8  3   �8     '9  .   =9  2   l9     �9     �9  7   �9     :     :     &:     @:     \:     t:  #   �:  #   �:     �:     �:     �:     ;     1;  
   L;     W;     s;     ;     �;  
   �;     �;     �;     �;     �;  '   �;     <     7<     N<  ,   _<  %   �<  .   �<     �<     �<  '   =  '   A=  0   i=     �=     �=  }   �=     T>     g>     z>     �>     �>     �>  #   �>  !   �>     !?     :?     K?  
   ^?     i?  
   ~?  �   �?  +   Z@     �@     �@  '   �@  	   �@  '   �@     A     "A     >A  ,   JA  *   wA      �A     �A     �A     �A     �A     B     2B     MB     ZB     tB     �B     �B     �B     �B     �B  (   �B  z   �B  
   zC     �C     �C     �C  '   �C     �C     �C     D  ,   (D     UD     rD  )   D  6   �D      �D     E     E     -E  "   CE     fE     �E     �E     �E     �E     �E     �E  %   F     .F     BF     TF  
   jF  &   uF     �F  �   �F     XG     fG     yG     �G     �G     �G     �G     �G     �G     H  G   H     _H     vH     �H     �H     �H     �H     �H     �H     �H     I     0I     7I     OI     kI  R   {I  T   �I  2   #J      VJ     wJ  1   �J  3   �J     �J  #   �J      K     4K     BK     NK     \K     pK  
   ~K  
   �K     �K  
   �K    by  %s Review %s Reviews A list of product categories. A list of your store's products. Account Settings Active filters Add All To Cart Add All To Wishlist Add video thumbnail Added Address: All All products Apply coupon Archives Attribute Attribute Type Audio Author Author URL Author: Average Average Rating Average rating Back To Shop Billing address Blog Bottom Brand: Brands Browse Compare Browse Wishlist Cancel Categories Box Category Layout Category order Category: Categories: Click here to enter your code Click here to login Click to open expanded view Close Coming Soon Page Comments are closed. Compare Content Count Coupon Discount Coupon code DESC Daily: Dashboard Date Default Delete Delete image Description Deviantart Display Settings Display a list of active product filters. Display a list of attributes to filter products in your store. Display a list of brands to filter products in your store. Display a list of star ratings to filter products in your store. Display links to social media networks. Don't Miss Out!  This promotion will expires in Don't show this popup again Edit Edit Account Elements Email Email address Email: Enter URL of Youtube or Vimeo or specific filetypes such as mp4, webm, ogv. Enter links for each banner here. Divide links with linebreaks (Enter). Enter the hashtag for which photos will be displayed. If no hashtag is entered, no photos will display. Enter your full address to see shipping costs. Featured Categories Featured products Filter Products by brands Filter by Filters First Name Fixed Follow us on social Forgot? Format Details Frequently Bought Together Full Width Gallery General Good Have a coupon? Hello! Hello, Hide Page Header Hide Text Hide Title Hide empty categories Hide free products Home HomePage Homepage Hot I am a customer I am a vendor I have read and agree to the <a target="_blank" href="%s">Terms &amp; Conditions</a>. ID Icon If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. Image In stock It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. It seems we can't find what you're looking for. Perhaps searching can help or go back to  Item Items Last Name Layout Leave a Reply to %s Left Link Loading Log In Your Account Log in Log out Login Logout Lost your password? Martfury - Currency Switcher Martfury - Filter Products by Attribute Martfury - Filter Products by Rating Martfury - Product Categories Martfury - Products Martfury - Social Links Martfury Addons Martfury Currency Switcher Martfury Product Tags Max height(px) Max height. Mega Menu Mega Menu Background Mega Menu Content Mega Width Menu Content Menu General Menu Icon Menu Order Middle Mobile Menu Monthly: My Wishlist N/A Name New Newer posts Newness Next Next (arrow right) Next Page Next Post No Repeat No Result found. No product categories exist. No products in the cart. Not Found Not that bad Nothing Found Nothing found Number of products to show Older posts On-sale products Only logged in customers who have purchased this product may leave a review. Order By Order by Orders Orders History Other Categories Page Header Background Page Header Settings Page Not Found Page Title Pages: Password Perfect Phone Number Phone: Please enter your username or email address. You will receive a link to create a new password via email. Plugin activated successfully. Popularity Portfolio Post Style Settings Previous Previous (arrow left) Previous Page Previous Post Price Price: high to low Price: low to high Primary Menu Product Product 360 View Product Carousel Product Tags Product Video Product categories Products Products Carousel Products found Quantity Quick View Quote Random Rate&hellip; Rated %s out of 5 Recently Viewed Products is a function which helps you keep track of your recent viewing history. Register Register An Account Remember me Remove Remove all filters Remove filter Remove image Remove this item Reset password Returning customer? Reviews Roll over image to zoom in Same Current Brand Same Current Category Search Box Search Icon Search Results Search for a product&hellip; Search results for Select Image Select Products Settings Share Shipping address Shop All Shop By Department Menu Shop Name Shop Now Shop URL Show Show hidden products Show product counts Sorry, but nothing matched your search terms. Please try again with some different keywords. Sort by Specification Star Status Status: Submit Review Submit your review Subtotal There are no reviews yet. This item: This product is currently out of stock and unavailable. Toggle fullscreen Top Top Categories Total Total Price:  Trending Update cart Upload/Add Images Upload/Add image Username or email View Visit Store Write your review here... You are here:  You have not set up this type of address yet. You must be <a href="%s">logged in</a> to post a review. You must be logged in to checkout. Your Email Your Name Your cart items Your comment is awaiting moderation. Your order Your rating of this product Your review is awaiting approval Zoom in/out by comments title%s Comments comments title1 Comment days hours minutes off seconds Project-Id-Version: Martfury
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 07:35+0000
PO-Revision-Date: 2019-01-03 06:23+0000
Last-Translator: root <mohmmedjunide73@gmail.com>
Language-Team: العربية
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100 >= 3 && n%100<=10 ? 3 : n%100 >= 11 && n%100<=99 ? 4 : 5;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ من قبل لا رأي رأي واحد رأيان %s رأي %s رأي %s رأي قائمة من تصنيفات المنتجات قائمة من منتجات متجرك خصائص الحساب فعل الفلاتر أضف الجميع للسلة أضف الجميع لقائمة الرغبات أصف صورة بارزة للفيديو تمت الإضافة العنوان: الكل كل المنتجات فعل الكوبون الأرشيف صفة نوع الصفة الصوت المؤلف رابط المؤلف المؤلف: متوسط متوسط التقييم متوسط التقييم العودة للمتجر عنوان الفوترة المدونة أسفل العلامة التجارية: العلامات التجارية تصفح المقارنات تصفح قائمة الرغبات ألغي صندوق التصنيف شكل التصميم ترتيب التصنيف :التصنيف التصنيف: التصنيفان: التصانيف: التصانيف:  إضغط هنا لتخل الكود الخاص بك انقر هنا لتسجيل الدخول انقر لفتح نافذة موسعة أغلق قريبا.. التعليقات مغلقة مقارنة محتوى عد خصم الكوبون الرمز السري للكوبون التفاصيل يومي: لوحة التحكم التاريخ: الإفتراضي إحذف إحذف الصورة التفاصيل الوسام
 اعدادات العرض
 عرض قائمة من عوامل تصفية المنتجات النشطة.
 عرض قائمة بالسمات لتصفية المنتجات في متجرك.
 عرض قائمة من العلامات التجارية لتصفية المنتجات في متجرك.
 عرض قائمة بتقييمات النجوم لتصفية المنتجات في متجرك.
 عرض الروابط لشبكات التواصل الاجتماعي.
 لا تفوت! هذا العرض سوف ينتهي في
 لا تظهر هذه النافذة المنبثقة مرة أخرى
 عدل عدل الحساب العناصر البريد الإلكتروني عنوان البريد الإلكتروني البريد الإلكتروني: أدخل الرابط URL لـ Youtube أو Vimeo لأنواع ملفات معينة مثل mp4 و webm و ogv.
 أدخل روابط لكل شعار هنا. قسّم الروابط مع فواصل الأسطر (أدخل).
 أدخل علامة التصنيف التي سيتم عرض الصور بها. إذا لم يتم إدخال علامة تصنيف ، فلن يتم عرض أي صور.
 أدخل عنوانك بالكامل لمشاهدة تكاليف الشحن.
 فئات مميزة
 منتجات مميزة
 تصفية المنتاجات حسب العلامة التجارية تصفية حسب
 مصفاة الاسم الاول
 ثابت
 تابعنا هل نسيت؟
 تفاصيل التنسيق
 تباع معاً في العادة العرض الكامل
 معرض الضور رئيسي جيد لديك كوبون ؟ مرحبا ! مرحبا,  إخفاء رأس الصفحة
 اخفي النص
 إخفاء العنوان
 إخفاء الفئات الفارغة
 إخفاء المنتجات المجانية
 الصفحة الرئيسية الصفحة الرئيسية
 الصفحة الرئيسية
 عروض ساخنة أنا زبون أنا بائع لقد قرأت وأوافق على <a target="_blank" href="٪s"> البنود & amp؛ شروط </A>.
 هوية أيقونة إذا كنت قد تسوقت معنا من قبل ، يرجى إدخال التفاصيل الخاصة بك في المربعات أدناه. إذا كنت عميلاً جديدًا ، فالرجاء المتابعة إلى قسم الفوترة والشحن.
 صورة
 متوفر  يبدو أنه لا يمكننا العثور على ما تبحث عنه. ربما البحث يمكن أن يساعد.
 يبدو أنه لا يمكننا العثور على ما تبحث عنه. ربما البحث يمكن أن يساعد أو يعود إلى
 مادة العناصر
 إسم العائلة التصميم أترك ردا ل %s يسار رابط جاري التحميل تسجيل الدخول دخول أخرج تسجيل الدخول الخروج فقدت كلمة المرور الخاصة بك؟
 مغير العملة تصفية المنتجات حسب السمة
 تصفية المنتجات حسب التصنيف
 فئات المنتجات
 المنتجات روابط مواقع التواصل الإجتماعي إضافات مغير العملة سمات المنتجات أقصى ارتفاع (px)
 اقصى ارتفاع.
 قائمة ميجا
 خلفية القائمة ميجا
 محتوى القائمة ميجا
 عرض ميجا
 محتوى القائمة
 القائمة العامة
 رمز القائمة
 ترتيب القائمة
 منتصف قائمة الموبايل شهريا: قائمة رغباتي نفذ المنتج الإسم جديد منشورات أحدث الحداثة التالي التالي (السهم الأيمن)
 الصفحة التالية المقبل بوست
 لا تكرار
 لم يتم العثور على نتائج.
 لا توجد فئات منتجات.
 لا توجد منتجات في العربة.
 غير معثور عليه
 ليس بهذا السوء
 لم يتم العثور على شيء
 لم يتم العثور على شيء
 عدد المنتجات المراد عرضها
 أقدم المشاركات
 منتجات على الخصم العملاء المسجلون الذين اشتروا هذا المنتج فقط بإمكانهم ترك رأيهم فيه. ترتيب حسب
 ترتيب حسب
 الطلبات تاريخ الطلبات تصنيفات أخرى خلفية رأس الصفحة
 إعدادات رأس الصفحة
 الصفحة غير موجودة
 عنوان الصفحة
 الصفحات:
 كلمه السر
 ممتاز رقم الهاتف
 هاتف:
 الرجاء إدخال اسم المستخدم أو عنوان البريد الإلكتروني. ستتلقى رابطًا لإنشاء كلمة مرور جديدة عبر البريد الإلكتروني. تم تفعيل اللإضافة بنجاح شعبية
 ملف الإنجازات إعدادات نمط المشاركة
 سابق
 السابق (السهم الأيسر)
 الصفحة السابقة الصفحة السابقة السعر
 السعر الاعلى الى الادنى
 السعر من الارخص للاعلى
 القائمة الأساسية
 المنتج عرض 360 للمنتج عارض المنتج علامات المنتج
 فيديو المنتج
 فئات المنتجات
 المنتج عارض المنتجات منتج عثر عليه الكمية عرض سريع إقتباس عشوائي التقييم... تم تقييمه بنسبة٪ s من 5
 عرض المنتجات الحديثة يبقسك على اطلاع بآخر المنتجات التي استعرضتها. تسجيل تسجيل حساب تذكرنى
 إزالة
 إزالة جميع التصفييات
 إزالة الفلتر
 إزالة الصورة
 حذف هذا المنتج إعادة تعيين كلمة المرور
 الزبون العائد؟
 الآراء حرك فوق الصورة للتكبير نفس العلامة التجارية الحالية
 نفس الفئة الحالية مربع البحث أيقونة البحث نتائج البحث ابحث عن منتج و hellip؛ نتائج البحث عن اختر صورة حدد المنتجات الإعدادات شارك مع الاصدقاء عنوان الشحن تسوق كل شيء تسوق حسب قائمة القسم اسم المتجر تسوق الآن رابط المتجر أظهار عرض المنتجات المخفية عرض عدد المنتجات عذرًا ، ولكن لا شيء يتطابق مع عبارات بحثك. أرجو المحاولة مرة أخرى بإستخدام كلمات أخرى.
 رتب حسب المواصفات نجمة تتبع الطلب تتبع الطلب: تسليم الرأي تسليم رأيك المجموع لا توجد آراء بعد هذا المنتج: هذا المنتج غير متوفر في المخزون حالياً. إملأ الشاشة  الأعلى أعلى تصنيفات المجموع السعر الكلي: الشائع تحديث عربة حمل \ أضيف صورة حمل \ أضيف صورة اسم المستخدم عرض زيارة المتجر اكتب رأيك هنا... أنت هنا:
 لم تقم بإعداد هذا النوع من العناوين حتى الآن.
 يجب عليك <a href="%s">تسجيل الدخول</a> قبل كتابة رأيك. يجب عليك تسجيل الدخول للدفع بريدك الالكتروني
 إسمك عناصر سلة التسوق الخاصة بك
 تعليقك ينتظر الموافقة عليه.
 طلبك تقييمك لهذا المنتج
 رأيك قيد الموافقة كبر/صغر من قبل %s تعليق تعليق واحد الأيام: ساعات دقائق خصم ثواني 