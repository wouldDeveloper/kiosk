Martfury - WooCommerce Marketplace WordPress Theme
------------------------------

Martfury is a WooCommerce Marketplace WordPress Theme. The theme is WooCommerce Ready and will help you build any kind of online store. This theme is suited for any type of website, e-commerce or personal or business use.


Change log:

Version 1.0.5

- Add new blog masonry layout.
- Add top bar for all header layout.
- Fix Some bugs about CSS.
- Fix Product categories display wrong.

Version 1.0.4

- Add WC Marketplace Compatible
- Add sidebar for pages
- Add new menu location for vendor, user logged
- Fix some bugs about CSS

Version 1.0.3

- Add: WC Vendors and WC Vendors Pro Compatible
- Add: Product features description in the single product.

Version 1.0.2

- Add: Social login in the register.
- Fix: Search everything with AJAX.
- Fix: Remove numbers in the share social.

Version 1.0.1

- Update: Visual Composer 5.4.7
- Update: Slider Revolution 5.4.7.1

Version 1.0
- Initial
